<script language="Javascript">
    function IsEmpty(){ 
        if(document.getElementById("form_surat_masuk").jenis_naskah.value == "")
        {
            alert("Jenis Naskah harus diisi");
            return false;
        }
    }
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Nota Dinas</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $page_title;?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="form_surat_masuk" onsubmit="return IsEmpty()" role="form" method="POST" action="index.php/admin/jenis_naskah/save">
                            <input type="hidden", name="id_surat_masuk", value=<?php echo $data == "" ? "" : $data[0]['id'];?>>
                            <div class="form-group">
                                <label for="disabledSelect">Tanggal Registrasi</label>
                                <input class="form-control" id="disabledInput" type="text" value=<?php echo date("d-m-Y"); ?> disabled>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_surat">Tanggal Surat *</label>
                                <input type="text" class="form-control" id="tanggal_surat" name="tanggal_surat">
                            </div>
                            <div class="form-group">
                                <label>Nomor Surat Unit Kerja *</label>
                                <input class="form-control" name="no_surat_unit_kerja" value=<?php echo $data == "" ? "" : $data[0]['no_surat_unit_kerja'];?>>
                            </div>
                            <div class="form-group">
                                <label>Nomor Agenda *</label>
                                <input class="form-control" name="no_agenda" value=<?php echo $data == "" ? "" : $data[0]['no_agenda'];?>>
                            </div>
                            <div class="form-group">
                                <label>Hal *</label>
                                <label>
                                    <input class="form-control" name="hal" value=<?php echo $data == "" ? "" : $data[0]['hal'];?>>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Tujuan Surat Keluar *</label>
                                <label>
                                    <input class="form-control" name="tujuan_surat_keluar" value=<?php echo $data == "" ? "" : $data[0]['tujuan_surat_keluar'];?>>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Kepada *</label>
                                <input class="form-control" name="tujuan_surat_keluar" value=<?php echo $data == "" ? "" : $data[0]['tujuan_surat_keluar'];?>>
                            </div>

                            <fieldset>
                                <legend>> Berkas</legend>
                                <div class="form-group">
                                    <label>Unit Kerja *</label>
                                    <input class="form-control" name="unit_kerja" value=<?php echo $data == "" ? "" : $data[0]['unit_kerja'];?>>
                                </div>
                                <div class="form-group">
                                    <label>Klasifikasi *</label>
                                    <input class="form-control" name="klasifikasi" value=<?php echo $data == "" ? "" : $data[0]['unit_kerja'];?>>
                                </div>
                                <div class="form-group">
                                    <div class="row-lg-5">
                                        <label>Nomor Berkas *</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input class="form-control" name="unit_kerja" value=<?php echo $data == "" ? "" : $data[0]['unit_kerja'];?>>
                                    </div>
                                    <div class="col-lg-5">
                                        <input class="form-control" name="unit_kerja" value=<?php echo $data == "" ? "" : $data[0]['unit_kerja'];?>>
                                    </div>
                                    <br/><br/>
                                </div>
                                <div class="form-group">
                                    <label>Judul Berkas *</label>
                                    <input class="form-control" name="judul_berkas" value=<?php echo $data == "" ? "" : $data[0]['unit_kerja'];?>>
                                </div>
                                <div class="form-group">
                                    <label>Attach Berkas</label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label>Isi Ringkas</label>
                                    <input class="form-control" name="judul_berkas" value=<?php echo $data == "" ? "" : $data[0]['unit_kerja'];?>>
                                </div>
                            </fieldset>

                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Daftar Jenis Naskah
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th width="10%" >No</th>
                            <th>Jenis Naskah</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            foreach ($rows as $jenis_naskah) {
                                echo "<tr class='even gradeX'>";
                                echo "<td>$no</td>";
                                echo "<td>$jenis_naskah->jenis_naskah</td>";
                                echo "<td align='center'><a href='index.php/admin/jenis_naskah/update/$jenis_naskah->id'>Ubah </a> | <a href='index.php/admin/jenis_naskah/delete/$jenis_naskah->id'>Hapus</a></td></tr>";
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->