<!DOCTYPE html>
<html>
  <head>
  	<base href="<?php echo base_url(); ?>">
	<base src="<?php echo base_url(); ?>">

    <title>Bootstrap Admin Theme v3</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href="vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="css/forms.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Bootstrap Admin Theme</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form">
	                       <input type="text" class="form-control" placeholder="Search...">
	                       <span class="input-group-btn">
	                         <button class="btn btn-primary" type="button">Search</button>
	                       </span>
	                  </div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.html">Profile</a></li>
	                          <li><a href="login.html">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php/documents"><i class="glyphicon glyphicon-tasks"></i> Dokumen</a></li>
                    <li><a href="index.html"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li class="current"><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="signup.html">Signup</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">

	  			<div class="row">
	  				<div class="col-md-15">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title">Horizontal Form</div>
					          
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
			  					<form class="form-horizontal" role="form">
								  <div class="form-group">
								    <label for="document_name" class="col-sm-3 control-label">Nama Dokumen</label>
								    <div class="col-sm-7">
								      <input class="form-control" placeholder="Text field" type="text">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="documen_from" class="col-sm-3 control-label">Dari</label>
								    <div class="col-sm-7">
								      <input class="form-control" placeholder="Text field" type="text">
								    </div>
								  </div>
  								  <div class="form-group">
								    <label for="documen_from" class="col-sm-3 control-label">Kategori</label>
		  							<div class="col-sm-7">
		  							<select class="selectpicker">
									    <option>Kategori A</option>
									    <option>Kategori B</option>
									    <option>Kategori C</option>
									  </select>
									 </div>
								  </div>
  								  <div class="form-group">
								    <label for="documen_from" class="col-sm-3 control-label">Keterangan</label>
								    <div class="col-sm-7">
								      <textarea class="form-control" placeholder="Textarea" rows="3"></textarea>
								    </div>
								  </div>
  								  <div class="form-group">
  								  	<label for="documen_from" class="col-sm-3 control-label">File</label>
								    <div class="col-sm-7">
										<input type="file" class="btn btn-default" id="exampleInputFile1">
								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button type="submit" class="btn btn-primary">Simpan</button>
								    </div>
								  </div>
								</form>
			  				</div>

			  				<div class="panel-heading">
								<legend>List Dokumen</legend>
							</div>
			  				<div class="panel-body">
			  					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
									<thead>
										<tr>
											<th>Nama Dokumen</th>
											<th>Dari</th>
											<th>Kategori</th>
											<th align="center">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr class="odd gradeX">
											<td>Ebook Marketplace</td>
											<td>Wina Anggara</td>
											<td>Ebook</td>
											<td align="center"><a href="index.php/documents/">Input Related Document</a> | Download | Edit | Delete</td>
										</tr>
										<tr class="even gradeC">
											<td>How to be a good PM</td>
											<td>Yuni Prasasti</td>
											<td>Ebook</td>
											<td align="center">Input Related Document | Download | Edit | Delete</td>
										</tr>
										<tr class="odd gradeA">
											<td>Scrum Implementation</td>
											<td>David Sinaga</td>
											<td>Ebook</td>
											<td align="center">Input Related Document | Download | Edit | Delete</td>
										</tr>
										<tr class="even gradeA">
											<td>Finance Report 2016</td>
											<td>Komala Ratnasari</td>
											<td>Document</td>
											<td align="center">Input Related Document | Download | Edit | Delete</td>
										</tr>
									</tbody>
								</table>
			  				</div>
			  			</div>
			  			</div>
	  				</div>
	  				


	  		<!--  Page content -->
		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="vendors/form-helpers/js/bootstrap-formhelpers.min.js"></script>

    <script src="vendors/select/bootstrap-select.min.js"></script>

    <script src="vendors/tags/js/bootstrap-tags.min.js"></script>

    <script src="vendors/mask/jquery.maskedinput.min.js"></script>

    <script src="vendors/moment/moment.min.js"></script>

    <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

     <!-- bootstrap-datetimepicker -->
     <link href="vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">
     <script src="vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script> 


    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <script src="js/custom.js"></script>
    <script src="js/forms.js"></script>
  </body>
</html>