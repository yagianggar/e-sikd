<script language="Javascript">
    function IsEmpty(){ 
        if(document.getElementById("form_tingkat_urgensi").tingkat_urgensi.value == "")
        {
            alert("Tingkat Urgensi harus diisi");
            return false;
        }
    }
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Data Master</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $page_title;?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="form_tingkat_urgensi" onsubmit="return IsEmpty()" role="form" method="POST" action="index.php/admin/tingkat_urgensi/save">
                            <div class="form-group">
                                <label>Tingkat Urgensi *</label>
                                <input type="hidden", name="id_tingkat_urgensi", value=<?php echo $data == "" ? "" : $data[0]['id'];?>>
                                <input class="form-control" name="tingkat_urgensi" value=<?php echo $data == "" ? "" : $data[0]['tingkat_urgensi'];?>>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Daftar Tingkat Urgensi
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th width="10%" >No</th>
                            <th>Tingkat Urgensi</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            foreach ($rows as $tingkat_urgensi) {
                                echo "<tr class='even gradeX'>";
                                echo "<td>$no</td>";
                                echo "<td>$tingkat_urgensi->tingkat_urgensi</td>";
                                echo "<td align='center'><a href='index.php/admin/tingkat_urgensi/update/$tingkat_urgensi->id'>Ubah </a> | <a href='index.php/admin/tingkat_urgensi/delete/$tingkat_urgensi->id'>Hapus</a></td></tr>";
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->