<script language="Javascript">
    function IsEmpty(){
        var form_pengguna = document.getElementById("form_pengguna");
        if(form_pengguna.nama.value == "") {
            alert("Nama harus diisi");
            return false;
        }

        if(form_pengguna.username.value == "") {
            alert("Username harus diisi");
            return false;
        }

        if(form_pengguna.password.value == "") {
            alert("Password harus diisi");
            return false;
        }

        if(form_pengguna.email.value == "") {
            alert("Email harus diisi");
            return false;
        }
    }
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Data Master</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $page_title;?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="form_pengguna" onsubmit="return IsEmpty()" role="form" method="POST" action="index.php/admin/users/save">
                            <div class="form-group">
                                <label>Nama Lengkap *</label>
                                <input type="hidden", name="user_id", value=<?php echo $data == "" ? "" : $data[0]['id'];?>>
                                <input class="form-control" name="nama" value="<?php echo $data == "" ? "" : $data[0]['nama'];?>">
                            </div>
                            <div class="form-group">
                                <label>Username *</label>
                                <input class="form-control" name="username" value=<?php echo $data == "" ? "" : $data[0]['username'];?>>
                            </div>
                            <div class="form-group">
                                <label>Password *</label>
                                <input class="form-control" name="password" value=<?php echo $data == "" ? "" : $data[0]['password'];?>>
                            </div>
                            <div class="form-group">
                                <label>Email *</label>
                                <input class="form-control" name="email" value=<?php echo $data == "" ? "" : $data[0]['email'];?>>
                            </div>
                            <div class="form-group">
                                <label>Jabatan *</label>
                                <select class="form-control" name="id_grup_jabatan">
                                    <?php
                                        foreach ($jabatan_rows as $jabatan) {
                                            if ($data != "") {
                                                if ($jabatan->id == $data[0]['id_grup_jabatan']) {
                                                    echo "<option value='$jabatan->id' selected>$jabatan->grup_jabatan</option>";
                                                } else {
                                                    echo "<option value='$jabatan->id'>$jabatan->grup_jabatan</option>";
                                                }
                                            } else {
                                                echo "<option value='$jabatan->id'>$jabatan->grup_jabatan</option>";
                                            }
                                        }
                                    ?>
                                    
                                </select>
                            </div>

                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Daftar Pengguna
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th width="10%" >No</th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Email</th>
                            <th>Jabatan</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            foreach ($rows as $pengguna) {
                                echo "<tr class='even gradeX'>";
                                echo "<td>$no</td>";
                                echo "<td>$pengguna->nama</td>";
                                echo "<td>$pengguna->username</td>";
                                echo "<td>$pengguna->password</td>";
                                echo "<td>$pengguna->email</td>";
                                echo "<td>$pengguna->grup_jabatan</td>";
                                echo "<td align='center'><a href='index.php/admin/users/update/$pengguna->id'>Ubah </a> | <a href='index.php/admin/users/delete/$pengguna->id'>Hapus</a></td></tr>";
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->