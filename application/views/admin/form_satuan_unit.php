<script language="Javascript">
    function IsEmpty(){ 
        if(document.getElementById("form_satuan_unit").satuan_unit.value == "")
        {
            alert("Satuan Unit harus diisi");
            return false;
        }
    }
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Data Master</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $page_title;?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="form_satuan_unit" onsubmit="return IsEmpty()" role="form" method="POST" action="index.php/admin/satuan_unit/save">
                            <div class="form-group">
                                <label>Satuan Unit *</label>
                                <input type="hidden", name="id_satuan_unit", value=<?php echo $data == "" ? "" : $data[0]['id'];?>>
                                <input class="form-control" name="satuan_unit" value=<?php echo $data == "" ? "" : $data[0]['satuan_unit'];?>>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Daftar Satuan Unit
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th width="10%" >No</th>
                            <th>Satuan Unit</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            foreach ($rows as $satuan_unit) {
                                echo "<tr class='even gradeX'>";
                                echo "<td>$no</td>";
                                echo "<td>$satuan_unit->satuan_unit</td>";
                                echo "<td align='center'><a href='index.php/admin/satuan_unit/update/$satuan_unit->id'>Ubah </a> | <a href='index.php/admin/satuan_unit/delete/$satuan_unit->id'>Hapus</a></td></tr>";
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->