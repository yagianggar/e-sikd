<script language="Javascript">
    function IsEmpty(){ 
        if(document.getElementById("form_grup_jabatan").grup_jabatan.value == "")
        {
            alert("Grup Jabatan harus diisi");
            return false;
        }
    }
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Data Master</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $page_title;?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="form_grup_jabatan" onsubmit="return IsEmpty()" role="form" method="POST" action="index.php/admin/grup_jabatan/save">
                            <div class="form-group">
                                <label>Grup Jabatan *</label>
                                <input type="hidden", name="id_grup_jabatan", value=<?php echo $data == "" ? "" : $data[0]['id'];?>>
                                <input class="form-control" name="grup_jabatan" value=<?php echo $data == "" ? "" : $data[0]['grup_jabatan'];?>>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Daftar Grup Jabatan
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th width="10%" >No</th>
                            <th>Grup Jabatan</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            foreach ($rows as $grup_jabatan) {
                                echo "<tr class='even gradeX'>";
                                echo "<td>$no</td>";
                                echo "<td>$grup_jabatan->grup_jabatan</td>";
                                echo "<td align='center'><a href='index.php/admin/grup_jabatan/update/$grup_jabatan->id'>Ubah </a> | <a href='index.php/admin/grup_jabatan/delete/$grup_jabatan->id'>Hapus</a></td></tr>";
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->