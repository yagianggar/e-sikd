<div class="col-md-10">
			<div class="row">
				<div class="col-md-15">
					<div class="content-box-large">
	  				<div class="panel-heading">
			            <legend><?php echo $page_title; ?></legend>
			          
			            <div class="panel-options">
			              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
			              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
			            </div>
			        </div>
	  				<div class="panel-body">
	  					<form class="form-horizontal" role="form">
						  <div class="form-group">
						    <label for="document_name" class="col-sm-3 control-label">Parent Document</label>
						    <div class="col-sm-7">
						      <span class="form-control">Read only text</span>
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="document_name" class="col-sm-3 control-label">Nama Dokumen</label>
						    <div class="col-sm-7">
						      <input class="form-control" placeholder="Text field" type="text">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="documen_from" class="col-sm-3 control-label">Dari</label>
						    <div class="col-sm-7">
						      <input class="form-control" placeholder="Text field" type="text">
						    </div>
						  </div>
							  <div class="form-group">
						    <label for="documen_from" class="col-sm-3 control-label">Kategori</label>
  							<div class="col-sm-7">
  							<select class="selectpicker">
							    <option>Kategori A</option>
							    <option>Kategori B</option>
							    <option>Kategori C</option>
							  </select>
							 </div>
						  </div>
							  <div class="form-group">
						    <label for="documen_from" class="col-sm-3 control-label">Keterangan</label>
						    <div class="col-sm-7">
						      <textarea class="form-control" placeholder="Textarea" rows="3"></textarea>
						    </div>
						  </div>
							  <div class="form-group">
							  	<label for="documen_from" class="col-sm-3 control-label">File</label>
						    <div class="col-sm-7">
								<input type="file" class="btn btn-default" id="exampleInputFile1">
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      <button type="submit" class="btn btn-primary">Simpan</button>
						    </div>
						  </div>
						</form>
	  				</div>

	  				<div class="panel-heading">
						<legend>List Dokumen</legend>
					</div>
	  				<div class="panel-body">
	  					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
							<thead>
								<tr>
									<th>Nama Dokumen</th>
									<th>Dari</th>
									<th>Kategori</th>
									<th align="center">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="odd gradeX">
									<td>Ebook Marketplace</td>
									<td>Wina Anggara</td>
									<td>Ebook</td>
									<td align="center">Download | Edit | Delete</td>
								</tr>
								<tr class="even gradeC">
									<td>How to be a good PM</td>
									<td>Yuni Prasasti</td>
									<td>Ebook</td>
									<td align="center">Download | Edit | Delete</td>
								</tr>
								<tr class="odd gradeA">
									<td>Scrum Implementation</td>
									<td>David Sinaga</td>
									<td>Ebook</td>
									<td align="center">Download | Edit | Delete</td>
								</tr>
								<tr class="even gradeA">
									<td>Finance Report 2016</td>
									<td>Komala Ratnasari</td>
									<td>Document</td>
									<td align="center">Download | Edit | Delete</td>
								</tr>
							</tbody>
						</table>
	  				</div>
	  			</div>
	  			</div>
				</div>
		<!--  Page content -->
  </div>
</div>