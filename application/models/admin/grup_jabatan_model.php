<?php
class grup_jabatan_model extends CI_Model {
	public function get_all() {
		$query = $this->db->get('master_grup_jabatan');
		return $query->result();
	}

	public function get_data_by_id($id) {
		$this->db->select('*');
		$this->db->from('master_grup_jabatan');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query;	
	}

	public function save_data($grup_jabatan) {
		$data = array(
			'grup_jabatan' => $grup_jabatan
		);
		$this->db->insert('master_grup_jabatan', $data);
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('master_grup_jabatan', $data); 
	}

	public function delete_by_id($id) {
		$this->db->where('id', $id);
		$this->db->delete('master_grup_jabatan');
	}
}