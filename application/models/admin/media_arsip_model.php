<?php
class Media_arsip_model extends CI_Model {
	public function get_all() {
		$query = $this->db->get('master_media_arsip');
		return $query->result();
	}

	public function get_data_by_id($id) {
		$this->db->select('*');
		$this->db->from('master_media_arsip');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query;	
	}

	public function save_data($media_arsip) {
		$data = array(
			'media_arsip' => $media_arsip
		);
		$this->db->insert('master_media_arsip', $data);
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('master_media_arsip', $data); 
	}

	public function delete_by_id($id) {
		$this->db->where('id', $id);
		$this->db->delete('master_media_arsip');
	}
}