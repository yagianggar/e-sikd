<?php
class Satuan_unit_model extends CI_Model {
	public function get_all() {
		$query = $this->db->get('master_satuan_unit');
		return $query->result();
	}

	public function get_data_by_id($id) {
		$this->db->select('*');
		$this->db->from('master_satuan_unit');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query;	
	}

	public function save_data($satuan_unit) {
		$data = array(
			'satuan_unit' => $satuan_unit
		);
		$this->db->insert('master_satuan_unit', $data);
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('master_satuan_unit', $data); 
	}

	public function delete_by_id($id) {
		$this->db->where('id', $id);
		$this->db->delete('master_satuan_unit');
	}
}