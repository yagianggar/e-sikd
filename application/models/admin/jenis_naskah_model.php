<?php
class Jenis_naskah_model extends CI_Model {
	public function get_all() {
		$query = $this->db->get('master_jenis_naskah');
		return $query->result();
	}

	public function get_data_by_id($id) {
		$this->db->select('*');
		$this->db->from('master_jenis_naskah');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query;	
	}

	public function save_data($jenis_naskah) {
		$data = array(
			'jenis_naskah' => $jenis_naskah
		);
		$this->db->insert('master_jenis_naskah', $data);
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('master_jenis_naskah', $data); 
	}

	public function delete_by_id($id) {
		$this->db->where('id', $id);
		$this->db->delete('master_jenis_naskah');
	}
}