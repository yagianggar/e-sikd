<?php
class Users_model extends CI_Model {
	public function get_all() {
		$this->db->select('users.*, master_grup_jabatan.id as id_jabatan, master_grup_jabatan.grup_jabatan');
		$this->db->from('users');
		$this->db->join('master_grup_jabatan', 'master_grup_jabatan.id = users.id_grup_jabatan');

		$query = $this->db->get();
		// $query = $this->db->get('users');
		return $query->result();
	}

	public function get_data_by_id($id) {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query;
	}

	public function save_data($data) {
		$this->db->insert('users', $data);
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
	}

	public function delete_by_id($id) {
		$this->db->where('id', $id);
		$this->db->delete('users');
	}

	public function get_user_by_username_password($username, $password) {
		$this->db->select('users.*, master_grup_jabatan.id as id_jabatan, master_grup_jabatan.grup_jabatan');
		$this->db->from('users');
		$this->db->join('master_grup_jabatan', 'master_grup_jabatan.id = users.id_grup_jabatan');
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));

		$user = $this->db->get();

		// $user = $this->db->get_where('users', array('username' => $username, 'password' => md5($password)));
		return $user;
	}
}