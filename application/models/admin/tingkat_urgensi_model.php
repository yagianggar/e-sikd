<?php
class Tingkat_urgensi_model extends CI_Model {
	public function get_all() {
		$query = $this->db->get('master_tingkat_urgensi');
		return $query->result();
	}

	public function get_data_by_id($id) {
		$this->db->select('*');
		$this->db->from('master_tingkat_urgensi');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query;	
	}

	public function save_data($tingkat_urgensi) {
		$data = array(
			'tingkat_urgensi' => $tingkat_urgensi
		);
		$this->db->insert('master_tingkat_urgensi', $data);
	}

	public function update_data($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('master_tingkat_urgensi', $data); 
	}

	public function delete_by_id($id) {
		$this->db->where('id', $id);
		$this->db->delete('master_tingkat_urgensi');
	}
}