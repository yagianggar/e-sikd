<?php
class Home extends CI_Controller {

    public function index() {
        if (isset($this->session->userdata['logged_in'])) {
            $jabatan = $this->session->userdata['logged_in']['grup_jabatan'];
            if ($jabatan == 'Administrator') {
                redirect('admin/tingkat_perkembangan');
            } else {
                redirect('surat');
            }
        } else {
            $this->load->view('form_login');
        }
    }

    public function login() {
        $this->load->model('admin/users_model');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->users_model->get_user_by_username_password($username, $password);

        if ($user->num_rows() > 0) {
            $session_data = array(
                    'nama' => $user->result()[0]->nama,
                    'username' => $user->result()[0]->username,
                    'email' => $user->result()[0]->email,
                    'id_grup_jabatan' => $user->result()[0]->id_grup_jabatan,
                    'grup_jabatan' => $user->result()[0]->grup_jabatan
                );
            $this->session->set_userdata('logged_in', $session_data);

            $jabatan = $this->session->userdata['logged_in']['grup_jabatan'];
            if ($jabatan == 'Administrator') {
                redirect('admin/tingkat_perkembangan');
            } else {
                redirect('surat');
            }

        } else {
            $page_details['msg'] = 'Username / Password anda salah';
            $this->load->view('form_login', $page_details);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->load->view('form_login');
    }

    public function is_logged_in() {
        echo isset($this->session->userdata['logged_in']);
    }

}