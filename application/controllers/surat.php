<?php
class Surat extends CI_Controller {
	public function __construct() {
	    parent::__construct();
        if (!isset($this->session->userdata['logged_in'])) {
            redirect('home');
        }
	    // $this->load->model('admin/sifat_naskah_model');
	}

	public function index() {
		// $results = $this->sifat_naskah_model->get_all();

		$page_details['page'] = 'welcome';
		$page_details['page_title'] = 'Welcome';
		$page_details['msg'] = "Selamat Datang ".$this->session->userdata['logged_in']['nama'];
		$this->load->view('templates', $page_details);
	}

	public function registrasi_surat_masuk() {
		// $results = $this->sifat_naskah_model->get_all();

		$page_details['page'] = 'form_surat_masuk';
		$page_details['page_title'] = 'Registrasi Nota Dinas';
		$page_details['rows'] = '';
		$page_details['data'] = "";
		$this->load->view('templates', $page_details);
	}

	public function save() {
		$id_sifat_naskah = $this->input->post('id_sifat_naskah');
		$sifat_naskah = $this->input->post('sifat_naskah');

		$data = array(
				'sifat_naskah' => $sifat_naskah
			);

		if ($id_sifat_naskah == null) {
			$this->sifat_naskah_model->save_data($sifat_naskah);
		} else {
			$this->sifat_naskah_model->update_data($id_sifat_naskah, $data);
		}

		redirect('admin/sifat_naskah');
	}

	public function update($id) {
		$result = $this->sifat_naskah_model->get_data_by_id($id);
		$results = $this->sifat_naskah_model->get_all();

		$page_details['page'] = 'admin/form_sifat_naskah';
		$page_details['page_title'] = 'Update Sifat Naskah';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->sifat_naskah_model->delete_by_id($id);
		redirect('admin/sifat_naskah');
	}
}