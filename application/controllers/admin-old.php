<?php
class Admin extends CI_Controller {

	public function index() {
		$this->load->view('admin/login');
	}

	public function home() {
		$page_details['page'] = 'admin/form_tingkat_perkembangan';
		$page_details['page_title'] = 'Tingkat Perkembangan';
		$this->load->view('admin/admin_template', $page_details);
	}

	public function tingkat_perkembangan() {
		$this->load->model('admin/tingkat_perkembangan');
		$results = $this->tingkat_perkembangan->get_all();

		$page_details['page'] = 'admin/form_tingkat_perkembangan';
		$page_details['page_title'] = 'Tingkat Perkembangan';
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete_tingkat_perkembangan() {
		
	}
}