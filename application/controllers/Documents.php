<?php
class Documents extends CI_Controller {

	public function index() {
		$pageDetails['page'] = 'document_input_form';
		$pageDetails['page_title'] = 'Form Input Dokumen';
		$this->load->view('templates', $pageDetails);
	}

	public function details() {
		$pageDetails['page'] = 'document_details_form';
		$pageDetails['page_title'] = 'Detail Dokumen';
		$this->load->view('templates', $pageDetails);
	}
}