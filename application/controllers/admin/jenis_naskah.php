<?php
class Jenis_naskah extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    $this->load->model('admin/jenis_naskah_model');
	}

	public function index() {
		$results = $this->jenis_naskah_model->get_all();

		$page_details['page'] = 'admin/form_jenis_naskah';
		$page_details['page_title'] = 'Jenis Naskah';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$id_jenis_naskah = $this->input->post('id_jenis_naskah');
		$jenis_naskah = $this->input->post('jenis_naskah');

		$data = array(
				'jenis_naskah' => $jenis_naskah
			);

		if ($id_jenis_naskah == null) {
			$this->jenis_naskah_model->save_data($jenis_naskah);
		} else {
			$this->jenis_naskah_model->update_data($id_jenis_naskah, $data);
		}

		redirect('admin/jenis_naskah');
	}

	public function update($id) {
		$result = $this->jenis_naskah_model->get_data_by_id($id);
		$results = $this->jenis_naskah_model->get_all();

		$page_details['page'] = 'admin/form_jenis_naskah';
		$page_details['page_title'] = 'Update Jenis Naskah';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->jenis_naskah_model->delete_by_id($id);
		redirect('admin/jenis_naskah');
	}
}