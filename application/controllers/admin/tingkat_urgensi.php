<?php
class Tingkat_urgensi extends CI_Controller {
	public function index() {
		$this->load->model('admin/tingkat_urgensi_model');
		$results = $this->tingkat_urgensi_model->get_all();

		$page_details['page'] = 'admin/form_tingkat_urgensi';
		$page_details['page_title'] = 'Tingkat Urgensi';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$this->load->model('admin/tingkat_urgensi_model');
		$id_tingkat_urgensi = $this->input->post('id_tingkat_urgensi');
		$tingkat_urgensi = $this->input->post('tingkat_urgensi');

		$data = array(
				'tingkat_urgensi' => $tingkat_urgensi
			);

		if ($id_tingkat_urgensi == null) {
			$this->tingkat_urgensi_model->save_data($tingkat_urgensi);
		} else {
			$this->tingkat_urgensi_model->update_data($id_tingkat_urgensi, $data);
		}

		redirect('admin/tingkat_urgensi');
	}

	public function update($id) {
		$this->load->model('admin/tingkat_urgensi_model');
		$result = $this->tingkat_urgensi_model->get_data_by_id($id);
		$results = $this->tingkat_urgensi_model->get_all();

		$page_details['page'] = 'admin/form_tingkat_urgensi';
		$page_details['page_title'] = 'Update Tingkat Urgensi';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->load->model('admin/tingkat_urgensi_model');
		$this->tingkat_urgensi_model->delete_by_id($id);
		redirect('admin/tingkat_urgensi');
	}
}