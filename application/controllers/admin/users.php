<?php
class Users extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    $this->load->model('admin/users_model');
	}

	public function index() {
		$this->load->model('admin/grup_jabatan_model');
		$results = $this->users_model->get_all();
		$jabatan_rows = $this->grup_jabatan_model->get_all();

		$page_details['page'] = 'admin/form_pengguna';
		$page_details['page_title'] = 'Pengguna';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$page_details['jabatan_rows'] = $jabatan_rows;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$user_id = $this->input->post('user_id');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email = $this->input->post('email');
		$id_grup_jabatan = $this->input->post('id_grup_jabatan');

		$data = array(
				'nama' => $nama,
				'username' => $username,
				'password' => md5($password),
				'email' => $email,
				'id_grup_jabatan' => $id_grup_jabatan
			);

		if ($user_id == null) {
			$this->users_model->save_data($data);
		} else {
			$this->users_model->update_data($user_id, $data);
		}

		redirect('admin/users');
	}

	public function update($id) {
		$this->load->model('admin/grup_jabatan_model');
		$jabatan_rows = $this->grup_jabatan_model->get_all();

		$result = $this->users_model->get_data_by_id($id);
		$results = $this->users_model->get_all();

		$page_details['page'] = 'admin/form_pengguna';
		$page_details['page_title'] = 'Update Informasi Pengguna';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$page_details['jabatan_rows'] = $jabatan_rows;
		
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->users_model->delete_by_id($id);
		redirect('admin/users');
	}
}