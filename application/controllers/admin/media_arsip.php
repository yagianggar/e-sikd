<?php
class Media_arsip extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    $this->load->model('admin/media_arsip_model');
	}

	public function index() {
		$results = $this->media_arsip_model->get_all();

		$page_details['page'] = 'admin/form_media_arsip';
		$page_details['page_title'] = 'Media Arsip';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$id_media_arsip = $this->input->post('id_media_arsip');
		$media_arsip = $this->input->post('media_arsip');

		$data = array(
				'media_arsip' => $media_arsip
			);

		if ($id_media_arsip == null) {
			$this->media_arsip_model->save_data($media_arsip);
		} else {
			$this->media_arsip_model->update_data($id_media_arsip, $data);
		}

		redirect('admin/media_arsip');
	}

	public function update($id) {
		$result = $this->media_arsip_model->get_data_by_id($id);
		$results = $this->media_arsip_model->get_all();

		$page_details['page'] = 'admin/form_media_arsip';
		$page_details['page_title'] = 'Update Media Arsip';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->media_arsip_model->delete_by_id($id);
		redirect('admin/media_arsip');
	}
}