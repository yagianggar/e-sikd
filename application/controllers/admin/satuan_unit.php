<?php
class Satuan_unit extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    $this->load->model('admin/satuan_unit_model');
	}

	public function index() {
		$results = $this->satuan_unit_model->get_all();

		$page_details['page'] = 'admin/form_satuan_unit';
		$page_details['page_title'] = 'Satuan Unit';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$id_satuan_unit = $this->input->post('id_satuan_unit');
		$satuan_unit = $this->input->post('satuan_unit');

		$data = array(
				'satuan_unit' => $satuan_unit
			);

		if ($id_satuan_unit == null) {
			$this->satuan_unit_model->save_data($satuan_unit);
		} else {
			$this->satuan_unit_model->update_data($id_satuan_unit, $data);
		}

		redirect('admin/satuan_unit');
	}

	public function update($id) {
		$result = $this->satuan_unit_model->get_data_by_id($id);
		$results = $this->satuan_unit_model->get_all();

		$page_details['page'] = 'admin/form_satuan_unit';
		$page_details['page_title'] = 'Update Satuan Unit';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->satuan_unit_model->delete_by_id($id);
		redirect('admin/satuan_unit');
	}
}