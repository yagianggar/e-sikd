<?php
class Tingkat_perkembangan extends CI_Controller {
	public function index() {
		$this->load->model('admin/tingkat_perkembangan_model');
		$results = $this->tingkat_perkembangan_model->get_all();

		$page_details['page'] = 'admin/form_tingkat_perkembangan';
		$page_details['page_title'] = 'Tingkat Perkembangan';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$this->load->model('admin/tingkat_perkembangan_model');
		$id_tingkat_perkembangan = $this->input->post('id_tingkat_perkembangan');
		$tingkat_perkembangan = $this->input->post('tingkat_perkembangan');

		$data = array(
				'tingkat_perkembangan' => $tingkat_perkembangan
			);

		if ($id_tingkat_perkembangan == null) {
			$this->tingkat_perkembangan_model->save_data($tingkat_perkembangan);
		} else {
			$this->tingkat_perkembangan_model->update_data($id_tingkat_perkembangan, $data);
		}

		redirect('admin/tingkat_perkembangan');
	}

	public function update($id) {
		$this->load->model('admin/tingkat_perkembangan_model');
		$result = $this->tingkat_perkembangan_model->get_data_by_id($id);
		$results = $this->tingkat_perkembangan_model->get_all();

		$page_details['page'] = 'admin/form_tingkat_perkembangan';
		$page_details['page_title'] = 'Update Tingkat Perkembangan';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->load->model('admin/tingkat_perkembangan_model');
		$this->tingkat_perkembangan_model->delete_by_id($id);
		redirect('admin/tingkat_perkembangan');
	}
}