<?php
class Grup_jabatan extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    if (!isset($this->session->userdata['logged_in'])) {
            redirect('home');
        }
		$this->load->model('admin/grup_jabatan_model');
	}

	public function index() {
		$results = $this->grup_jabatan_model->get_all();

		$page_details['page'] = 'admin/form_grup_jabatan';
		$page_details['page_title'] = 'Grup Jabatan';
		$page_details['rows'] = $results;
		$page_details['data'] = "";
		$this->load->view('admin/admin_template', $page_details);
	}

	public function save() {
		$id_grup_jabatan = $this->input->post('id_grup_jabatan');
		$grup_jabatan = $this->input->post('grup_jabatan');

		$data = array(
				'grup_jabatan' => $grup_jabatan
			);

		if ($id_grup_jabatan == null) {
			$this->grup_jabatan_model->save_data($grup_jabatan);
		} else {
			$this->grup_jabatan_model->update_data($id_grup_jabatan, $data);
		}

		redirect('admin/grup_jabatan');
	}

	public function update($id) {
		$result = $this->grup_jabatan_model->get_data_by_id($id);
		$results = $this->grup_jabatan_model->get_all();

		$page_details['page'] = 'admin/form_grup_jabatan';
		$page_details['page_title'] = 'Update Grup Jabatan';
		$page_details['data'] = $result->result_array();
		$page_details['rows'] = $results;
		$this->load->view('admin/admin_template', $page_details);
	}

	public function delete($id) {
		$this->grup_jabatan_model->delete_by_id($id);
		redirect('admin/grup_jabatan');
	}
}