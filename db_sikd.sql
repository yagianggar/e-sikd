/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.21-MariaDB : Database - db_sikd
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_sikd` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_sikd`;

/*Table structure for table `level_akses` */

DROP TABLE IF EXISTS `level_akses`;

CREATE TABLE `level_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_grup_jabatan` int(11) NOT NULL,
  `input` tinyint(1) NOT NULL,
  `ubah` tinyint(1) NOT NULL,
  `hapus` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_grup_jabatan` (`id_grup_jabatan`),
  KEY `id_grup_jabatan_2` (`id_grup_jabatan`),
  CONSTRAINT `level_akses_ibfk_1` FOREIGN KEY (`id_grup_jabatan`) REFERENCES `master_grup_jabatan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `level_akses` */

/*Table structure for table `master_grup_jabatan` */

DROP TABLE IF EXISTS `master_grup_jabatan`;

CREATE TABLE `master_grup_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_jabatan` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grup_jabatan` (`grup_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `master_grup_jabatan` */

insert  into `master_grup_jabatan`(`id`,`grup_jabatan`) values (8,'Administrator'),(2,'Eselon I'),(3,'Eselon II'),(4,'Eselon III'),(5,'Eselon IV'),(6,'Pimpinan Instansi'),(7,'Unit Kearsipan');

/*Table structure for table `master_jenis_naskah` */

DROP TABLE IF EXISTS `master_jenis_naskah`;

CREATE TABLE `master_jenis_naskah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_naskah` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jenis_naskah` (`jenis_naskah`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `master_jenis_naskah` */

insert  into `master_jenis_naskah`(`id`,`jenis_naskah`) values (1,'Surat Dinas'),(3,'Surat Undangan');

/*Table structure for table `master_media_arsip` */

DROP TABLE IF EXISTS `master_media_arsip`;

CREATE TABLE `master_media_arsip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_arsip` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_arsip` (`media_arsip`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `master_media_arsip` */

insert  into `master_media_arsip`(`id`,`media_arsip`) values (2,'Kertas');

/*Table structure for table `master_satuan_unit` */

DROP TABLE IF EXISTS `master_satuan_unit`;

CREATE TABLE `master_satuan_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_unit` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `satuan_unit` (`satuan_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `master_satuan_unit` */

insert  into `master_satuan_unit`(`id`,`satuan_unit`) values (2,'Halaman'),(3,'Lembar');

/*Table structure for table `master_sifat_naskah` */

DROP TABLE IF EXISTS `master_sifat_naskah`;

CREATE TABLE `master_sifat_naskah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sifat_naskah` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sifat_naskah` (`sifat_naskah`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `master_sifat_naskah` */

insert  into `master_sifat_naskah`(`id`,`sifat_naskah`) values (2,'Biasa'),(3,'Rahasia'),(4,'Rahasia Terbatas'),(5,'Sangat Rahasia');

/*Table structure for table `master_tingkat_perkembangan` */

DROP TABLE IF EXISTS `master_tingkat_perkembangan`;

CREATE TABLE `master_tingkat_perkembangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat_perkembangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tingkat_perkembangan` (`tingkat_perkembangan`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `master_tingkat_perkembangan` */

insert  into `master_tingkat_perkembangan`(`id`,`tingkat_perkembangan`) values (1,'Asli'),(13,'Kopi');

/*Table structure for table `master_tingkat_urgensi` */

DROP TABLE IF EXISTS `master_tingkat_urgensi`;

CREATE TABLE `master_tingkat_urgensi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat_urgensi` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tingkat_urgensi` (`tingkat_urgensi`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `master_tingkat_urgensi` */

insert  into `master_tingkat_urgensi`(`id`,`tingkat_urgensi`) values (2,'Amat Segera'),(3,'Biasa'),(4,'Segera');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `id_grup_jabatan` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_grup_jabatan` (`id_grup_jabatan`),
  CONSTRAINT `FK_grup_jabatan` FOREIGN KEY (`id_grup_jabatan`) REFERENCES `master_grup_jabatan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`nama`,`username`,`password`,`email`,`id_grup_jabatan`) values (1,'Yagi Anggar Prahara','yagianggar','687f225f7392a55f07b999aec191c461','yagi.anggar@gmail.com',5),(2,'Administrator','admin','0192023a7bbd73250516f069df18b500','admin@gmail.com',8);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
